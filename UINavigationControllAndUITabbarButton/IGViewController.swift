//
//  IGViewController.swift
//  UINavigationControllAndUITabbarButton
//
//  Created by JONNY on 8/30/1399 AP.
//

import UIKit

class IGViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    // Item Object
    var objItem = Item(image: ["1","2","3","4","5"],
                   name: ["Jonny","Mosani","Haita","Him","Leak"],
                   picture: ["2","4","1","5","3"],
                   likeCount: ["2308 likes","954 likes","567 likes","123 likes","2231 likes"],
                   desc: ["How are you doing","I'm really happy today","Hello How are you doing?","Keep going!!","Do you miss me?"],
                   commentProfile: ["1","2","3","4","5"])
    
    // Stroy Object
    var objStroy = Story(image: ["1","2","3","4","5"],
                         name: ["Jonny","Mosani","Haita","Him","Leak"])
    
    
    override func viewDidLoad() {
    
        super.viewDidLoad()
        
        // IG logo header
        let logo = UIImage(named: "logo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView

        tableView.delegate = self
        tableView.dataSource = self
        collectionView.delegate = self
        collectionView.dataSource = self
        
        
        // hide Scroll
        self.tableView.showsHorizontalScrollIndicator = false
        self.tableView.showsVerticalScrollIndicator = false
        
        self.collectionView.showsHorizontalScrollIndicator = false
        self.collectionView.showsVerticalScrollIndicator = false
        
        
        // Register nip file
        tableView.register(UINib(nibName: "ItemTableViewCell", bundle: nil), forCellReuseIdentifier: "ItemCell")
        collectionView.register(UINib(nibName: "StoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "StoryCell")

    }

}

extension IGViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objItem.name.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as! ItemTableViewCell
    
        cell.userProfileName.text = objItem.name[indexPath.row]
        cell.profileImage.image = UIImage(named:objItem.image[indexPath.row])
        cell.picture.image =  UIImage(named:objItem.picture[indexPath.row])
        cell.commentImage.image = UIImage(named:objItem.commentProfile[indexPath.row])
        cell.likeCount.text = objItem.likeCount[indexPath.row]
        cell.desc.text = objItem.desc[indexPath.row]
        
        // hide select for tableView
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        return cell
    }
    
    
    // post Item hight
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 640
    }
    
}

extension IGViewController :  UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return objStroy.name.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoryCell", for: indexPath) as! StoryCollectionViewCell
            
        cell.stroyProfile.image = UIImage(named: objStroy.image[indexPath.row]) 
        cell.stroyName.text = objStroy.name[indexPath.row]
        return cell
    }
    
}

// Stroy arangement
extension IGViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: 85, height: 150)
        }

}
