//
//  ItemTableViewCell.swift
//  UINavigationControllAndUITabbarButton
//
//  Created by JONNY on 9/1/1399 AP.
//

import UIKit

class ItemTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var commentImage: UIImageView!
    @IBOutlet weak var userProfileName: UILabel!
    @IBOutlet weak var likeCount: UILabel!
    @IBOutlet weak var desc: UILabel!
    
    @IBOutlet weak var love: UIButton!
    
    var isLove: Bool = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        // make image cicle
        profileImage.layer.cornerRadius = profileImage.frame.size.width/2
        profileImage.clipsToBounds = true
        
        commentImage.layer.cornerRadius = commentImage.frame.size.width/2
        commentImage.clipsToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    // Love button active
    @IBAction func loveAction(_ sender: Any) {
    
        if isLove{
            love.setImage(UIImage(named: "redHeart"), for: .normal)
            love.tintColor = .red
            love.borderColor = .red
            isLove = false
            
        }else{
            love.setImage(UIImage(named: "heart"), for: .normal)
            love.tintColor = .black
            isLove = true
        }
    }
    
}
