//
//  File.swift
//  UINavigationControllAndUITabbarButton
//
//  Created by BTB_015 on 11/21/20.
//

import Foundation

struct Item {
    var image: [String]
    var name: [String]
    var picture: [String]
    var likeCount: [String]
    var desc: [String]
    var commentProfile: [String]
}

struct Story {
    var image: [String]
    var name: [String]
}
